import pytest
from pytest import fail

from gaderypoluki.gaderypoluki import GaDeRyPoLuKi

# zbiór testów w postaci skryptu pytest

# fixture, czyli wcześniej przygotowany kawałek kodu do wielokrotnego uzycia
@pytest.fixture
def translator():
    translator = GaDeRyPoLuKi()
    return translator

# test właściwy
def test_test_should_translate(translator):
    # given
    msg = "lok"

    # when
    result = translator.translate(msg)

    # then
    assert result == "upi"

# uwaga na klauzulę wyłączającą test
@pytest.mark.skip
def test_should_stay_not_translated(translator):
    # given
    msg = "LOK"

    # when
    result = translator.translate(msg)

    # then
    fail("TODO")

@pytest.mark.skip
def test_should_translate2(translator):
    # given
    msg = "???"

    # when
    result = translator.translate(msg)

    # then
    fail("TODO")


def test_should_throw_exception(translator):
    # given

    # when
    with pytest.raises(Exception):
        translator.translate(None)

    # then

@pytest.mark.skip
def test_should_translate_ignore(translator):
    # given
    msg = "KOT"

    # when
    result = translator.translate_ignore_case(msg)

    # then
    fail("TODO")


def test_should_check_not_translatable(translator):
    # given
    c = "Z"

    # when
    result = translator.is_translatable(c)

    # then
    assert not result

@pytest.mark.skip
def test_should_check_translatable(translator):
    # given
    c = "g"

    # when
    result = translator.is_translatable(c)

    # then
    fail("TODO")

@pytest.mark.skip
def test_should_check_code_length(translator):
    # given

    # when
    size = translator.get_code_length()

    # then
    fail("TODO")
